<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package boxpress
 */

get_header(); ?>

  <?php require_once('template-parts/banners/banner--404.php'); ?>

  <section class="section">
    <div class="wrap wrap--limited">

      <article class="article-404">
        <header class="page-header">
          <h1 class="page-title">
            <?php _e( 'Sorry, this page can&rsquo;t be found.', 'boxpress' ); ?>
          </h1>
        </header>

        <div class="page-content">

          <p class="p-lead"><?php _e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'boxpress' ); ?></p>
          
          <div class="widget-404 widget-search">
            <?php get_template_part( 'template-parts/search-bar' ); ?>
          </div>

          <div class="l-columns-wrap">
            <div class="l-columns l-columns--2">
              <div class="l-column-item">
                <?php
                  the_widget( 'WP_Widget_Recent_Posts', array(), array(
                    'before_widget' => '<div class="widget-404 widget-recent-posts">',
                    'before_title' => '<h2 class="widget-title">',
                  ));
                ?>
                <div>
                  <a class="button" href="<?php echo get_permalink( get_option( 'page_for_posts' )); ?>"><?php _e('All Posts', 'boxpress'); ?></a>
                </div>
              </div>
              <div class="l-column-item">

                <?php if ( boxpress_categorized_blog() ) : ?>

                  <div class="widget-404 widget-categories">
                    <h2 class="widget-title"><?php _e( 'Most Used Categories', 'boxpress' ); ?></h2>
                    <ul>
                      <?php
                        wp_list_categories( array(
                          'orderby'    => 'count',
                          'order'      => 'DESC',
                          'show_count' => 1,
                          'title_li'   => '',
                          'number'     => 10,
                        ));
                      ?>
                    </ul>
                  </div>

                <?php endif; ?>

              </div>
            </div>
          </div>
        </div>
      </article>
    </div>
  </section>

<?php get_footer(); ?>
