<?php
/**
 * Template footer
 *
 * Contains the closing of the main el and all content after.
 *
 * @package boxpress
 */
?>


<?php
  $footer_copy = get_field( 'footer_copy', 'option' );
?>

</main>
<footer id="colophon" class="site-footer" role="contentinfo">
  <div class="wrap">
    <div class="primary-footer">
      <div class="l-footer l-footer--4-cols l-footer--gap-large">
        <div class="l-footer-item">
          <div class="site-branding">
            <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
              <span class="vh"><?php bloginfo('name'); ?></span>
              <svg class="site-logo" width="294" height="93" focusable="false">
                <use href="#footer-logo"/>
              </svg>
            </a>
          </div>
          <div class="footer-copy">
            <p><?php echo $footer_copy ?></p>
          </div>
        </div>
        <div class="l-footer-item l-footer-item--pull-right">
          <?php get_template_part( 'template-parts/global/address-block' ); ?>
        </div>
        <div class="l-footer-item">
          <p class="nav-title h5">
            <span itemprop="nav-title">Legal</span>
          </p>
          <?php if ( has_nav_menu( 'footer' )) : ?>
            <nav class="navigation--footer"
              aria-label="<?php _e( 'Utility Navigation', 'boxpress' ); ?>"
              role="navigation">
              <div class="header-contact-us">
                <h4 class="contact-header"><?php echo $header_title; ?>
                  <?php
              $tel_formatted = str_replace([ ".", "-", "–", "(", ")", " " ], '', $header_phone );
                   ?>
                  <p>
                    <span class="vh"><?php _e( 'Phone:', 'boxpress' ); ?></span>
                    <a href="tel:+1<?php echo $tel_formatted; ?>">
                      <span itemprop="telephone"><?php echo $header_phone; ?></span>
                    </a>
                  </p>
                </h4>
              </div>
              <ul class="nav-list">
                <?php
                  wp_nav_menu( array(
                    'theme_location'  => 'footer',
                    'items_wrap'      => '%3$s',
                    'container'       => false,
                    'walker'          => new Aria_Walker_Nav_Menu(),
                  ));
                ?>
              </ul>
            </nav>
          <?php endif; ?>
        </div>
        <div class="l-footer-item">
          <?php get_template_part( 'template-parts/global/social-nav' ); ?>
        </div>
      </div>
    </div>
    <div class="site-info">
      <div class="l-footer l-footer--2-cols l-footer--align-center">
        <div class="l-footer-item">
          <div class="site-copyright">
            <p>
              <small>
                <?php _e('Copyright', 'boxpress'); ?> &copy; <?php echo date('Y'); ?>
                <?php
                  $company_name     = get_bloginfo( 'name', 'display' );
                  $alt_company_name = get_field( 'alternative_company_name', 'option' );

                  if ( ! empty( $alt_company_name )) {
                    $company_name = $alt_company_name;
                  }
                ?>
                <?php echo $company_name; ?>.
                <?php _e('All rights reserved.', 'boxpress'); ?>
              </small>
            </p>
          </div>
        </div>
        <div class="l-footer-item l-footer-item--pull-right">
          <div class="imagebox">
            <p>
              <small>
                <?php _e('Website by', 'boxpress'); ?>
                <a href="https://imagebox.com" target="_blank">
                  <span class="vh">Imagebox</span>
                  <svg class="imagebox-logo-svg" width="81" height="23" focusable="false">
                    <use href="#imagebox-logo"/>
                  </svg>
                </a>
              </small>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
</div>

<?php wp_footer(); ?>

<?php // Footer Tracking Codes ?>
<?php the_field( 'footer_tracking_codes', 'option' ); ?>

</body>
</html>
