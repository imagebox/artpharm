<?php

  $third_bg_color = get_sub_field('third_bg_color');
  $onethird_copy = get_sub_field('onethird_copy');
  $twothird_copy = get_sub_field('twothird_copy');

 ?>

<section class="section thirds-block <?php echo $third_bg_color; ?>">
 <div class="wrap">
   <div class="one-third">
       <?php echo $onethird_copy; ?>
   </div>
   <div class="two-third">
     <?php echo $twothird_copy; ?>
   </div>
 </div>
</section>
