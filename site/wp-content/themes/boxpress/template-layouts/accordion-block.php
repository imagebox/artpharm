<?php
/**
 * Displays the accordion block layout
 *
 * @package boxpress
 */

?>

<section class="accordion-layout section <?php the_sub_field( 'accordion_background' ); ?>">
	<div class="wrap">

		<h2 class="accordion-section-heading"><?php the_sub_field( 'accordion_heading' ); ?></h2>
    <?php the_sub_field('accordion_text') ?>

		<?php if ( have_rows( 'accordion' ) ) : ?>
			<div class="js-accordion accordion">
			<?php
			while ( have_rows( 'accordion' ) ) :
					the_row();
				?>
				<h3 class="accordion-title">
					<button type="button"><?php the_sub_field( 'acc_title' ); ?></button>
				</h3>
				<div class="accordion-content">
					<?php the_sub_field( 'acc_text' ); ?>
				</div>
			<?php endwhile; ?>

			</div>
		<?php endif; ?>

	</div>
</section>
