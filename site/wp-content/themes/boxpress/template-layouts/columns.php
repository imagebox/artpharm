<?php
/**
 * Displays the column layout
 *
 * @package boxpress
 */

$column_heading = get_sub_field( 'column_section_heading' );
$column_heading_alignment = get_sub_field( 'column_section_heading_alignment' );
$column_row_alignment = get_sub_field( 'column_row_alignment' );
$column_bkg = get_sub_field( 'column_background' );

?>
<section class="column-layout section <?php echo $column_bkg; ?>">
  <div class="wrap">

    <?php if ( ! empty( $column_heading )) : ?>
      <header class="section-header section-header--<?php echo $column_heading_alignment; ?>">
        <h2><?php echo $column_heading; ?></h2>
      </header>
    <?php endif; ?>

    <?php if ( have_rows( 'column_row' ) ) : ?>
      <?php while ( have_rows( 'column_row' ) ) : the_row();
          $total_cols = get_sub_field( 'number_of_columns' );
        ?>

        <div class="l-columns l-columns--<?php echo $total_cols; ?> l-columns--align-<?php echo $column_row_alignment; ?>">

          <?php for ( $i = 1; $i <= $total_cols; $i++ ) :
              $column_content = get_sub_field( 'column_' . $i );
            ?>
            <?php if ( $column_content ) : ?>

              <div class="l-column-item">
                <div class="page-content">

                  <?php echo $column_content; ?>
                  
                </div>
              </div>

            <?php endif; ?>
          <?php endfor; ?>
          
        </div>

      <?php endwhile; ?>
    <?php endif; ?>

  </div>
</section>
