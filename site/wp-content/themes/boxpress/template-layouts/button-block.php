<?php
/**
 * Displays the button block layout
 *
 * @package boxpress
 */

$button_block_title = get_sub_field( 'button_block_title' );
$button_block_copy  = get_sub_field( 'button_block_copy' );
$button_block_link  = get_sub_field( 'button_block_link' );
$button_block_bkg   = get_sub_field( 'button_block_background' );
$button_block_bkg_image = get_sub_field( 'button_block_background_image' );
$button_block_bkg_size  = 'block_full_width';

?>
<section class="button-block-layout section <?php echo $button_block_bkg; ?>">
  <div class="wrap wrap--limited">
    <div class="button-block">

       <div class="callout-content">
         <?php if ( ! empty( $button_block_copy )) : ?>
           <?php echo $button_block_copy; ?>
         <?php endif; ?>
       </div>

    </div>
  </div>
  <?php if ( $button_block_bkg === 'background-image' && $button_block_bkg_image ) : ?>
    <img class="button-block-layout-bkg" draggable="false" aria-hidden="true"
      src="<?php echo esc_url( $button_block_bkg_image['url'] ); ?>"
      width="<?php echo esc_attr( $button_block_bkg_image['width' ] ); ?>"
      height="<?php echo esc_attr( $button_block_bkg_image['height' ] ); ?>"
      alt="">
  <?php endif; ?>
</section>
