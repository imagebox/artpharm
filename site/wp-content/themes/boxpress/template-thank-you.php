<?php
/**
 * Template Name: Thank You
 *
 * @package boxpress
 */

$child_pages_list = query_for_child_page_list();

?>
<?php get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>

  <section class="fullwidth-column section">
    <div class="wrap wrap--limtied">


        <div class="l-main-col">
         <?php get_template_part( 'template-parts/content/content', 'thanks' ); ?>
        </div>
    </div>
  </section>

<?php get_footer(); ?>
