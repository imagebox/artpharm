(function ($) {
  'use strict';

  /**
   * Accordion Controls
   */

  var $accordion = $('.js-accordion');

  $accordion.each(function () {
    var $this = $(this);
    var $accordion_button = $this.find('.accordion-title > button[type="button"]');
    var $accordion_slide  = $this.find('.accordion-content');
    var accordion_index   = $accordion.index( this ) + 1;

    // Prep Accordion
    $this.addClass('accordion-enabled');
    $accordion_slide.slideUp();

    // Prep Each Accordion Group
    $accordion_button.each(function () {
      var $this       = $(this);
      var this_index  = $accordion_button.index( this ) + 1;
      var group_id    = 'accordion-' + accordion_index + '-g-' + this_index;

      // Set button group ID
      $this.attr( 'aria-controls', group_id )
        .attr( 'aria-expanded', 'false' );

      // Set slide group ID
      $this.parent('.accordion-title')
        .next( '.accordion-content' )
        .attr( 'id', group_id );
    });

    // Toggle on click
    $accordion_button.on('click', function () {
      var $group_title    = $(this);
      var $group_content  = $group_title.parent('.accordion-title').next('.accordion-content');

      toggle_accordion_group(
        $group_title,
        $group_content,
        $accordion_button,
        $accordion_slide
      );

      return false;
    });

    // Toggle on enter key
    $accordion_button.on('keyup', function ( e ) {
      var enter_key = 13;

      if ( e.which == enter_key ) {
        var $group_title    = $(this);
        var $group_content  = $group_title.parent('.accordion-title').next('.accordion-content');

        toggle_accordion_group(
          $group_title,
          $group_content,
          $accordion_button,
          $accordion_slide
        );
      }
    });
  });

  /**
   * Toggle Accordion Group
   * @param  {jquery object} $button      The title to click or press
   * @param  {jquery object} $content     The content to open
   * @param  {jquery object} $all_titles  All title to toggle off
   * @param  {jquery object} $all_content All content to toggle off
   */
  function toggle_accordion_group( $button, $content, $all_titles, $all_content ) {
    if ( ! $button.hasClass( 'open' )) {

      // Close all groups
      $all_titles.removeClass( 'open' )
        .attr( 'aria-expanded', 'false' );
      $all_content.slideUp();

      // Open current group
      $button.addClass( 'open' )
        .attr( 'aria-expanded', 'true' );
      $content.slideDown();
    } else {

      // Close all groups
      $all_titles.removeClass( 'open' )
        .attr( 'aria-expanded', 'false' );
      $all_content.slideUp();
    }
  }

})(jQuery);
