<?php
/**
 * WordPress Editor related functions
 *
 * @package boxpress
 */

/**
 * Editor Formats
 */

function boxpress_mce_buttons( $buttons ) {
  array_unshift( $buttons, 'styleselect' );
  return $buttons;
}
add_filter( 'mce_buttons_2', 'boxpress_mce_buttons' );

function boxpress_mce_before_init_insert_formats( $init_array ) {
  $style_formats = array(
    array(
      'title'     => 'Lead Paragraph',
      'selector'  => 'p',
      'classes'   => 'p-lead',
    ),
    array(
      'title'     => 'h1 Style',
      'selector'  => 'p',
      'classes'   => 'h1',
    ),
    array(
      'title'     => 'Button Green',
      'selector'  => 'a',
      'classes'   => 'ip-button-green',
    ),
    array(
      'title'     => 'Button Orange',
      'selector'  => 'a',
      'classes'   => 'ip-button-orange',
    ),
    array(
      'title'     => 'Button Orange Light',
      'selector'  => 'a',
      'classes'   => 'ip-button-orange--light',
    ),
    array(
      'title'     => 'Button White',
      'selector'  => 'a',
      'classes'   => 'ip-button-white',
    ),
  );
  $init_array['style_formats'] = json_encode( $style_formats );

  return $init_array;
}
add_filter( 'tiny_mce_before_init', 'boxpress_mce_before_init_insert_formats' );



/**
 * Editor Styles
 */

function boxpress_theme_add_editor_styles() {
  add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'boxpress_theme_add_editor_styles' );
