<?php
/**
 * Template header
 *
 * This is the template that displays all of the <head> section and everything up until <main>
 *
 * @package boxpress
 */
?>


<?php
  $header_title = get_field( 'header_title', 'option' );
  $header_phone = get_field( 'header_phone', 'option' );
?>


<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <script>
    // No js Fallback - Remove 'no-js', add 'js' to html el
    !function(){var n=document.documentElement.classList;n.contains('no-js')&&(n.remove('no-js'),n.add('js'));}();
  </script>

  <?php wp_head(); ?>

  <?php // Header Tracking Codes ?>
  <?php the_field( 'header_tracking_codes', 'option' ); ?>

</head>
<body <?php body_class(); ?>>

<?php // Skip Link ?>
<a class="site-skip-link" href="#main"><?php _e( 'Skip to main content', 'boxpress' ); ?></a>

<?php // SVGs ?>
<?php include( get_template_directory() . '/template-parts/global/svg.php' ); ?>

<?php // Mobile Header ?>
<?php include( get_template_directory() . '/template-parts/global/header-mobile.php' ); ?>

<div class="site-wrap">
<header id="masthead" class="site-header" role="banner">
  <div class="wrap">

    <div class="l-header">
      <div class="l-header-col-1">

        <div class="site-branding">
          <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
            <span class="vh"><?php bloginfo('name'); ?></span>
            <svg class="site-logo" width="254" height="81" focusable="false">
              <use href="#site-logo"/>
            </svg>
          </a>
        </div>

      </div>
      <div class="l-header-col-2">

        <div class="site-navigation">
          <?php ?>
          <?php if ( has_nav_menu( 'secondary' )) : ?>
            <nav class="navigation--utility"
              aria-label="<?php _e( 'Utility Navigation', 'boxpress' ); ?>"
              role="navigation">
              <div class="header-contact-us">
                <h4 class="contact-header"><?php echo $header_title; ?>:
                  <?php
              $tel_formatted = str_replace([ ".", "-", "–", "(", ")", " " ], '', $header_phone );
                   ?>
                 </h4>
                  <p>
                    <span class="vh"><?php _e( 'Phone:', 'boxpress' ); ?></span>
                    <a class="header-phone" href="tel:+1<?php echo $tel_formatted; ?>">
                      <span itemprop="telephone"><?php echo $header_phone; ?></span>
                    </a>
                  </p>
              </div>
              <ul class="nav-list">
                <?php
                  wp_nav_menu( array(
                    'theme_location'  => 'secondary',
                    'items_wrap'      => '%3$s',
                    'container'       => false,
                    'walker'          => new Aria_Walker_Nav_Menu(),
                  ));
                ?>
              </ul>
            </nav>
          <?php endif; ?>

          <?php // Main Navigation ?>
          <?php if ( has_nav_menu( 'primary' )) : ?>
            <nav class="js-accessible-menu navigation--main"
              aria-label="<?php _e( 'Main Navigation', 'boxpress' ); ?>"
              role="navigation">
              <ul class="nav-list">
                <?php
                  wp_nav_menu( array(
                    'theme_location'  => 'primary',
                    'items_wrap'      => '%3$s',
                    'container'       => false,
                    'walker'          => new Aria_Walker_Nav_Menu(),
                  ));
                ?>
              </ul>
            </nav>
          <?php endif; ?>

        </div>

      </div>
    </div>

  </div>
</header>
<main id="main" class="site-main" role="main">
