<?php
/**
 * Displays the Social Navigation
 *
 * @package boxpress
 */

$social_nav_group_ID  = 77; // Change to the ID of your Social Nav field group
$social_nav_fields    = acf_get_raw_fields( $social_nav_group_ID );
$social_nav_links     = array();

// Create a new object of only links. This lets us hide this block
// when the social nav fields exist, but no links are set.
if ( $social_nav_fields ) {
  foreach ( $social_nav_fields as $field ) {
    $field_object = get_field_object( $field['name'], 'option' );

    if ( $field_object['value'] && ! empty( $field_object['value'] )) {
      $social_nav_links[] = array(
        'title' => $field_object['label'],
        'url'   => $field_object['value'],
        'name'  => $field_object['name'],
      );
    }
  }
}
?>
<?php if ( $social_nav_links ) : ?>

  <div class="navigation--social">
    <h5 class="social-nav-title"><?php _e('Follow Us', 'boxpress'); ?></h5>

    <ul class="social-nav">

      <?php foreach ( $social_nav_links as $link ) : ?>

        <li>
          <a href="<?php echo esc_url( $link['url'] ); ?>" target="_blank" rel="nofollow noopener">
            <span class="vh"><?php echo $link['title']; ?></span>
            <svg class="social-<?php echo $link['name']; ?>-svg" width="50" height="50" focusable="false">
              <use href="#social-<?php echo $link['name']; ?>"/>
            </svg>
          </a>
        </li>

      <?php endforeach; ?>

    </ul>
  </div>

<?php endif; ?>
