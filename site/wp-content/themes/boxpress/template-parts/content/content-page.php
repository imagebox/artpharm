<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package boxpress
 */

$media_headline = get_field( 'media_headline' );
// If media headline is set, use it as the page title
$page_title     = ( ! empty( $media_headline )) ? $media_headline : get_the_title();

?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'content--page' ); ?>>
  <header class="page-header">
    <h1 class="page-title"><?php echo $page_title; ?></h1>
  </header>
  <div class="page-content">
    <?php the_content(); ?>
  </div>
</article>
