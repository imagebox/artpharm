<?php
/**
 * Displays the blog banner
 *
 * @package boxpress
 */

$banner_title = get_the_title( get_option( 'page_for_posts', true ));
$banner_image_url = '';
$default_banner   = get_field( 'default_banner_image', 'option' );
$blog_banner      = get_field( 'blog_banner_image', 'option' );

if ( $blog_banner ) {
  $banner_image_url = $blog_banner['url'];
} elseif ( $default_banner ) {
  $banner_image_url = $default_banner['url'];
}

?>
<header class="banner">
  <div class="wrap">
    <div class="banner-title">
      <span class="h1">
        <?php echo $banner_title; ?>
      </span>
    </div>
  </div>
  <div class="banner-photo-block">
    <div class="inner-photo">
      <?php if ( ! empty( $banner_image_url )) : ?>
        <img class="banner-image" draggable="false" src="<?php echo $banner_image_url; ?>" alt="">
      <?php endif; ?>
    </div>
  </div>
</header>
