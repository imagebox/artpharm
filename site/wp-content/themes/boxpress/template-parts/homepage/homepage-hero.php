<?php
/**
 * Displays the Hero layout
 *
 * Easily convertible into a sideshow by enabling multiple
 * rows in the repeater.
 *
 * @package boxpress
 */
?>
<?php if ( have_rows( 'homepage_hero' )) : ?>
  <?php while ( have_rows( 'homepage_hero' )) : the_row();
      $hero_heading     = get_sub_field( 'hero_heading' );
      $hero_subheading  = get_sub_field( 'hero_subheading' );
      $hero_link        = get_sub_field( 'hero_link' );
      $hero_background  = get_sub_field( 'hero_background' );
      $hero_background_image = get_sub_field( 'hero_background_image' );
    ?>

    <section class="hero <?php echo $hero_background; ?>">
      <div class="wrap">
        <div class="hero-box">
          <div class="hero-content">

            <h1><?php echo $hero_heading; ?></h1>

            <?php if ( ! empty( $hero_subheading )) : ?>
              <p><?php echo $hero_subheading; ?></p>
            <?php endif; ?>

            <?php if ( $hero_link ) : ?>
              <?php
                $hero_link_target = ! empty( $hero_link['target'] ) ? $hero_link['target'] : '_self';
              ?>
              <a class="button-one"
                href="<?php echo esc_url( $hero_link['url'] ); ?>"
                target="<?php echo esc_attr( $hero_link_target ); ?>">
                <?php echo $hero_link['title']; ?>
              </a>
            <?php endif; ?>

          </div>
        </div>
      </div>

      <?php if ( $hero_background_image && $hero_background === 'background-image' ) : ?>
        <img class="hero-bkg-image" draggable="false" aria-hidden="true"
          src="<?php echo esc_url( $hero_background_image['url'] ); ?>"
          width="<?php echo esc_attr( $hero_background_image['width'] ); ?>"
          height="<?php echo esc_attr( $hero_background_image['height'] ); ?>"
          alt="<?php echo esc_attr( $hero_background_image['alt'] ); ?>">
      <?php endif; ?>
    </section>

  <?php endwhile; ?>
<?php endif; ?>
