
    <?php
/**
 * Displays the Slideshow layout
 *
 * @package boxpress
 */

  $home_carousel_photo  = get_field( 'home_carousel_photo' );
  $home_carousel_mobile_background = get_field( 'home_carousel_mobile_background' );
?>

<?php
/**
 * Displays the Slideshow layout
 *
 * @package boxpress
 */

  $home_carousel_photo  = get_field( 'home_carousel_photo' );
  $home_carousel_mobile_background = get_field( 'home_carousel_mobile_background' );
?>

<section class="section home-quote-section">
  <?php if ( $home_carousel_photo ) : ?>
    <img class="homepage-quote-section-desktop-bkg" draggable="false" aria-hidden="true"
      src="<?php echo esc_url( $home_carousel_photo['url'] ); ?>"
      width="<?php echo esc_attr( $home_carousel_photo['width'] ); ?>"
      height="<?php echo esc_attr( $home_carousel_photo['height'] ); ?>"
      alt="<?php echo esc_attr( $home_carousel_photo['alt'] ); ?>">
  <?php endif; ?>

  <?php if ( $home_carousel_mobile_background ) : ?>
    <img class="homepage-quote-section-mobile-bkg" draggable="false" aria-hidden="true"
      src="<?php echo esc_url( $home_carousel_mobile_background['url'] ); ?>"
      width="<?php echo esc_attr( $home_carousel_mobile_background['width'] ); ?>"
      height="<?php echo esc_attr( $home_carousel_mobile_background['height'] ); ?>"
      alt="<?php echo esc_attr( $home_carousel_mobile_background['alt'] ); ?>">
  <?php endif; ?>

  <div class="wrap wrap--limited">
    <?php if ( have_rows( 'quote_carousel' )) : ?>
      <div class="home-carousel">
        <div class="quote-carousel-home">
          <?php while ( have_rows( 'quote_carousel' )) : the_row(); ?>
            <?php
              $quote  = get_sub_field( 'quote' );
              $name  = get_sub_field( 'name' );
             ?>

            <div class="carousel-slide">
              <div class="slide-content">
                <svg class="quote-logo" width="61" height="61" focusable="false">
                  <use href="#quote-icon"/>
                </svg>
                <div class="quote-body">
                  <?php if ( $quote ) : ?>
                    <h2><?php echo $quote; ?></h2>
                  <?php endif; ?>

                  <?php if ( $name ) : ?>
                    <span> <?php echo $name; ?></span>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          <?php endwhile; ?>
        </div>
      </div>
    <?php endif; ?>
  </div>
</section>
