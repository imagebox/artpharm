
    <?php
/**
 * Displays the Slideshow layout
 *
 * @package boxpress
 */

  $form_header  = get_field( 'form_header' );

?>


<section class="section home-newsletter-section">
  <div class="wrap">
   <div class="l-grid l-grid--two-col">
     <div class="l-grid-item">
       <div class="form-header">
         <h2><?php echo $form_header; ?></h2>
       </div>
     </div>
    <div class="l-grid-item">
      <div class="form-inputs">
       <?php echo gravity_form(1, false, false, false, '', true, 12);  ?>
      </div>
    </div>
   </div>
  </div>
</section>
