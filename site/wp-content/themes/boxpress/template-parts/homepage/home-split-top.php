<?php
/**
 * Displays the slipt content / image layout
 *
 * @package boxpress
 */

$home_top_split_content    = get_field( 'home_top_split_content' );
$home_top_split_image      = get_field( 'home_top_split_image' );
$home_top_split_image_size = 'block_half_width';

?>
<section class=" section home-top-split-section split-block-layout">
  <div class="split-block-col">
    <div class="split-block-content">
      <div class="split-block-content-inner-wrap">
        <?php echo $home_top_split_content; ?>
        <?php if ( have_rows( 'home_split_top_shortcut_links' )) : ?>
          <ul>
          <?php while ( have_rows( 'home_split_top_shortcut_links' )) : the_row();
           $link = get_sub_field('link');
          ?>
           <li>
             <?php if ( $link ) : ?>
               <?php
                 $link_target = ! empty( $link['target'] ) ? $link['target'] : '_self';
               ?>
               <a class="button"
                 href="<?php echo esc_url( $link['url'] ); ?>"
                 target="<?php echo esc_attr( $link_target ); ?>">
                 <?php echo $link['title']; ?>
               </a>
             <?php endif; ?>
           </li>
          <?php endwhile; ?>
        </ul>
         <?php endif; ?>
      </div>
    </div>
  </div>

  <div class="split-block-col">
    <?php if ( $home_top_split_image ) : ?>
      <img class="split-block-image"
        src="<?php echo esc_url( $home_top_split_image['url'] ); ?>"
        width="<?php echo esc_attr( $home_top_split_image['width'] ); ?>"
        height="<?php echo esc_attr( $home_top_split_image['height'] ); ?>"
        alt="<?php echo esc_attr( $home_top_split_image['alt'] ); ?>">
    <?php endif; ?>
  </div>
</section>
