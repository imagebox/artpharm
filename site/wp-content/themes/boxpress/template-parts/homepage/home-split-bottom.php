<?php
/**
 * Displays the slipt content / image layout
 *
 * @package boxpress
 */

$home_bottom_split_content    = get_field( 'home_bottom_split_content' );
$home_bottom_split_image      = get_field( 'home_bottom_split_image' );
$home_bottom_split_image_size = 'block_half_width';

?>
<section class="section split-block-layout home-bottom-split-section split-block-layout--bottom">
  <div class="split-block-col">
    <?php if ( $home_bottom_split_image ) : ?>
      <img class="split-block-image"
      src="<?php echo esc_url( $home_bottom_split_image['url'] ); ?>"
      width="<?php echo esc_attr( $home_bottom_split_image['width'] ); ?>"
      height="<?php echo esc_attr( $home_bottom_split_image['height'] ); ?>"
      alt="<?php echo esc_attr( $home_bottom_split_image['alt'] ); ?>">
    <?php endif; ?>
  </div>

  <div class="split-block-col">
    <div class="split-block-content">
      <div class="split-block-content-inner-wrap">
        <?php echo $home_bottom_split_content; ?>
      </div>
    </div>
  </div>

</section>
