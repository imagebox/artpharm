<?php
  $home_alert_pic = get_field('home_alert_pic');
  $home_alert_heading = get_field('home_alert_heading');
  $home_alert_text = get_field('home_alert_text');
  $home_alert_link = get_field('home_alert_link');
 ?>


<section class="section home-alert-split-section">
 <div class="wrap">
  <div class="alert-box">
    <?php if ( $home_alert_link ) : ?>
      <?php
      $home_alert_link_target = ! empty( $home_alert_link['target'] ) ? $home_alert_link['target'] : '_self';
      ?>
      <a
      href="<?php echo esc_url( $home_alert_link['url'] ); ?>"
      target="<?php echo esc_attr( $home_alert_link_target ); ?>">
    <div class="l-two-thirds">
      <div class="l-one-third-col">
        <div class="photo-box">
          <?php if ( $home_alert_pic ) : ?>
            <img src="<?php echo esc_url( $home_alert_pic['url'] ); ?>"
            width="<?php echo esc_attr( $home_alert_pic['width'] ); ?>"
            height="<?php echo esc_attr( $home_alert_pic['height'] ); ?>"
            alt="<?php echo esc_attr( $home_alert_pic['alt'] ); ?>">
          <?php endif; ?>
        </div>
      </div>

        <div class="l-two-third-col">
            <div class="content">
              <h2><?php echo $home_alert_heading; ?></h2>

              <div class="alert-footer">
                <?php if ( $home_alert_text ) : ?>
                  <p><?php echo $home_alert_text; ?></p>
                <?php endif; ?>

                  <div class="button">
                    <?php echo $home_alert_link['title']; ?>
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</a>
<?php endif; ?>
</div>
</section>
