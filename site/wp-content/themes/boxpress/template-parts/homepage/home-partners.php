
    <?php
/**
 * Displays the Slideshow layout
 *
 * @package boxpress
 */

  $partner_header  = get_field( 'partner_header' );

?>


<section class="section home-partner-section">
  <div class="wrap">
    <h2><?php echo $partner_header; ?></h2>
      <?php if ( have_rows( 'partner_carousel' )) : ?>
        <div class="home-carousel">
          <div class="partner-carousel-home">
            <?php while ( have_rows( 'partner_carousel' )) : the_row(); ?>
              <?php
              $partner_logo  = get_sub_field( 'partner_logo' );
              $partner_logo_link  = get_sub_field( 'partner_logo_link' );
              ?>

              <div class="carousel-slide l-grid-item">
                <div class="slide-content">
                    <?php if ( $partner_logo_link ) : ?>
                      <?php
                        $partner_logo_link_target = ! empty( $partner_logo_link['target'] ) ? $partner_logo_link['target'] : '_self';
                      ?>
                      <a
                        href="<?php echo esc_url( $partner_logo_link['url'] ); ?>"
                        target="<?php echo esc_attr( $button_block_link_target ); ?>">
                        <?php echo $partner_logo_link['title']; ?>
                          <img
                          src="<?php echo esc_url( $partner_logo['url'] ); ?>"
                          width="<?php echo esc_attr( $partner_logo['width'] ); ?>"
                          height="<?php echo esc_attr( $partner_logo['height'] ); ?>"
                          alt="<?php echo esc_attr( $partner_logo['alt'] ); ?>">
                      </a>

                    <?php else: ?>
                        <img
                        src="<?php echo esc_url( $partner_logo['url'] ); ?>"
                        width="<?php echo esc_attr( $partner_logo['width'] ); ?>"
                        height="<?php echo esc_attr( $partner_logo['height'] ); ?>"
                        alt="<?php echo esc_attr( $partner_logo['alt'] ); ?>">
                    <?php endif; ?>

                </div>
              </div>
            <?php endwhile; ?>
            </div>
          </div>
      <?php endif; ?>
  </div>
</section>
