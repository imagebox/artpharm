<section class="section home-card-section">
  <div class="wrap">
    <div class="l-grid l-grid--three-col">
      <?php if ( have_rows( 'home_callout_column' )) : ?>
        <?php while ( have_rows( 'home_callout_column' )) : the_row();
          $home_callout_column_icon = get_sub_field('home_callout_column_icon');
          $home_callout_column_heading = get_sub_field('home_callout_column_heading');
          $home_callout_column_text = get_sub_field('home_callout_column_text');
          $home_callout_column_link = get_sub_field('home_callout_column_link');
          ?>
            <div class="l-grid-item">
              <div class="card">
                <?php
                  $home_callout_column_link_target = ! empty( $home_callout_column_link['target'] ) ? $home_callout_column_link['target'] : '_self';
                ?>
                <?php if ( $home_callout_column_link ) : ?>
                  <a  href="<?php echo esc_url( $home_callout_column_link['url'] ); ?>"
                  target="<?php echo esc_attr( $home_callout_column_link['target'] ); ?>">
                  <div class="card-header">
                    <?php if ( $home_callout_column_icon ) : ?>
                      <img
                        src="<?php echo esc_url( $home_callout_column_icon['url'] ); ?>"
                        width="<?php echo esc_attr( $home_callout_column_icon['width'] ); ?>"
                        height="<?php echo esc_attr( $home_callout_column_icon['height'] ); ?>"
                        alt="<?php echo esc_attr( $home_callout_column_link['alt'] ); ?>">
                    <?php endif; ?>
                    <h2><?php echo $home_callout_column_heading; ?></h2>
                    <p>
                      <?php echo $home_callout_column_text; ?>
                    </p>
                  </div>
                  <div class="card-footer">
                    <span class="button"><?php echo $home_callout_column_link['title']; ?></span>
                  </div>
                </a>
              <?php endif; ?>
              </div>
            </div>
          <?php endwhile; ?>
        <?php endif; ?>
      </div>
  </div>
</section>
