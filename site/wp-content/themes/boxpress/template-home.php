<?php
/**
 * Template Name: Homepage
 *
 * Page template to display the homepage.
 *
 * @package boxpress
 */
get_header(); ?>

  <article class="homepage">

    <?php // Hero ?>
    <?php get_template_part( 'template-parts/homepage/homepage-hero' ); ?>
    <?php get_template_part( 'template-parts/homepage/home-alert' ); ?>
    <?php get_template_part( 'template-parts/homepage/home-split-top' ); ?>
    <?php get_template_part( 'template-parts/homepage/home-columns' ); ?>
    <?php get_template_part( 'template-parts/homepage/home-quote' ); ?>
    <?php get_template_part( 'template-parts/homepage/home-split-bottom' ); ?>
    <?php get_template_part( 'template-parts/homepage/home-partners' ); ?>
    <?php get_template_part( 'template-parts/homepage/home-signup' ); ?>

  </article>

<?php get_footer(); ?>
